import os
import time
import htcondor

from ipyparallel.apps.launcher import BaseLauncher, ClusterAppMixin

from traitlets import (
    Any, Integer, CFloat, List, Unicode, Dict, Instance, HasTraits, CRegExp
)


class LDASCondorLauncher(BaseLauncher):
    def _submit_ad(self):
        run_args = '--profile-dir={profile_dir} --cluster-id={cluster_id}'.format(
            profile_dir=self.profile_dir,
            cluster_id=self.cluster_id,
            )
        self.log.debug("run args %s: %r", self.__class__.__name__, run_args)
        submit_log = os.path.join(self.profile_dir, 'htcondor_submit.log')
        # FIXME: must be str for python2
        submit_log = str(submit_log)
        self.log.debug("log path %s: %r", self.__class__.__name__, submit_log)
        ad = {
            'universe': 'vanilla',
            'executable': self._executable,
            'arguments': run_args,
            'transfer_executable': 'False',
            'log': submit_log,
            'accounting_group': 'cit.test',
            'Requirements': 'TARGET.General_Use_AMD =?= True',
            '+General_Use_AMD': 'True',
            }
        return ad

    def find_args(self):
        return [self.profile_dir, self.cluster_id]

    def start(self, n):
        htcondor.set_subsystem("TOOL")
        htcondor.param['TOOL_DEBUG'] = 'D_FULLDEBUG'
        htcondor.param['TOOL_LOG'] = str(os.path.join(self.profile_dir, 'htcondor.log'))
        htcondor.enable_log()
        htcondor.enable_debug()
        self.log.debug("Starting %s: %r", self.__class__.__name__, self.args)
        ad = self._submit_ad()
        sub = htcondor.Submit(ad)
        schedd = htcondor.Schedd()
        with schedd.transaction() as txn:
            job_id = sub.queue(txn, count=n)
        self.job_id = job_id
        self.notify_start(job_id)
        return job_id

    def stop(self):
        htcondor.Schedd().act(htcondor.JobAction.Remove,
                              'ClusterId == {}'.format(self.job_id))



class LDASCondorControllerLauncher(LDASCondorLauncher, ClusterAppMixin):
    _name = 'controller'
    def start(self):
        return super(LDASCondorControllerLauncher, self).start(1)

class LDASCondorEngineSetLauncher(LDASCondorLauncher, ClusterAppMixin):
    _name = 'engine'
    def start(self, n):
        # FIXME: HACK: THIS BLOCKS THE SINGLE-USER SERVER!  need a
        # better way to asynchronously wait for the controller process
        # to start before launching engines.
        ej = os.path.join(self.profile_dir, 'security', 'ipcontroller-engine.json')
        self.log.debug("Waiting for ipcontroller-engine.json to appear...")
        while not os.path.exists(ej):
            time.sleep(0.1)
        return super(LDASCondorEngineSetLauncher, self).start(n)
