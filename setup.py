#!/usr/bin/env python3

from setuptools import setup

setup(
    name = 'ldas_condor_launcher',
    version = '0.1',
    description = 'ipyparallel BaseLauncher for Condor',
    author = 'Jameson Graef Rollins',
    author_email = 'jameson.rollins@ligo.org',
    url = 'https://git.ligo.org/jameson.rollins/ipyparallel_ldas',
    license = 'GPLv3+',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        # maybe extend this?  "assword gui" won't work on anything but
        # X11, but the rest of it might still be useful.
        'Operating System :: POSIX',
        ],
    install_requires = [
        'ipyparallel',
        ],
)
