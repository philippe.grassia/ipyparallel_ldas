ipyparallel_ldas
================

[ipyparallel](https://ipyparallel.readthedocs.io/en/latest/)
BaseLauncher for launching controllers and engines via Condor on the
LIGO LDAS cluster.

Once installed, the following variables must be set in the profile
ipcluster_config.py file:

```
c.IPClusterEngines.engine_launcher_class = 'ldas_condor_launcher.LDASCondorEngineSetLauncher'
c.IPClusterStart.controller_launcher_class = 'ldas_condor_launcher.LDASCondorControllerLauncher'
```
